interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    // badge: {
    //   variant: 'info',
    //   text: 'NEW'
    // },
  },
  {
    title: true,
    name: 'Components'
  },
  {
    divider: true
  },
  {
    name: 'Limite',
    url: '/limite',
    icon: 'icon-map'
  },
  {
    name: 'Categorias',
    url: '/categorias',
    icon: 'icon-grid'
  },
  {
    name: 'Establecimientos',
    url: '/establecimientos',
    icon: 'icon-book-open '
  },
  {
    name: 'Usuarios',
    url: '/users',
    icon: 'icon-people',
  },
  {
    name: 'Roles',
    url: '/roles',
    icon: 'icon-docs'
  },
  {
    name: 'Favoritos',
    url: '/favoritos',
    icon: 'icon-badge'
  },
  {
    name: 'Productos',
    url: '/productos',
    icon: 'icon-handbag'
  },
  {
    name: 'Publicidades',
    url: '/publicidades',
    icon: 'icon-picture'
  },
  // {
  //   name: 'Sugerencias',
  //   url: '/sugerencias',
  //   icon: 'icon-graph'
  // },
  {
    name: 'Delivery',
    url: '/delivery',
    icon: 'icon-location-pin'
  }
];
