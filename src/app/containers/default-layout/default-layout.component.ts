import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems } from '../../_nav';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { TokenService } from '../../services/token.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  constructor(
    private auth: AuthService,
    private router: Router,
    private token: TokenService,

    @Inject(DOCUMENT) _document?: any) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
    console.log(this.navItems);
    
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  logout(event: MouseEvent) {
    
    let headers = new HttpHeaders({
      'Accept': 'application/json;charset=utf-8',
    'Content-Type': 'application/json;charset=utf-8',
      'Authorization': `Bearer ${this.token.get()}`
    });
    event.preventDefault();
    this.auth.changeAuthStatus(false);
    this.auth.logout(headers);
    this.router.navigateByUrl('/login');
  }
}
