import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileSizePipe } from '../pipes/file-size.pipe';
import { ObservableFilterPipe } from '../pipes/observable-filter.pipe';



@NgModule({
  declarations: [
    FileSizePipe,
    ObservableFilterPipe
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    FileSizePipe,
    ObservableFilterPipe
  ]
})
export class SharedModule { }
