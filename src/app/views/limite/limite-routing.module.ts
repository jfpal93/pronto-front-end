import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LimiteComponent } from './limite.component';


const routes: Routes = [
  {
    path: '',
    component: LimiteComponent,
    data: {
      title: 'Limite'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LimiteRoutingModule {}
