import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { LimiteComponent } from './limite.component';
import { LimiteRoutingModule } from './limite-routing.module';
import { DeliveryServiceService } from '../../services/delivery-service.service';
import { AccordionGroupComponent } from '../../components/accordion/accordion-group.component';
import { AccordionComponent } from '../../components/accordion/accordion.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    CdkTableModule,
    LimiteRoutingModule,
    NgbModule,
    FormsModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey:
      'AIzaSyAt7Mk0F78UZ5Oj33N0IPpZE8LgauIugYM',
      libraries: ['drawing']
    })
  ],
  declarations: [
    LimiteComponent,
   ],
  exports: []
})
export class LimiteModule { }
