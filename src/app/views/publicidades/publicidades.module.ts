import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { PublicidadesRoutingModule } from './publicidades-routing.module';
import { PublicidadesComponent } from './publicidades.component';
import { PublicidadesService } from '../../services/publicidades.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PublicidadesRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
   PublicidadesComponent
   ],
  exports: [],
  providers: [
   PublicidadesService
  ]
})
export class PublicidadesModule { }
