import { Component, OnInit } from '@angular/core';
import { PublicidadesService } from '../../services/publicidades.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, finalize } from 'rxjs/operators';
import { CategoriasService } from '../../services/categorias.service';
import { ProductosService } from '../../services/productos.service';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';

@Component({
  selector: 'app-publicidades',
  templateUrl: './publicidades.component.html',
  styleUrls: ['./publicidades.component.scss']
})
export class PublicidadesComponent implements OnInit {

  publicidad = {
    id: null,
    nombre: null,
    linkImagen: null,
    detalle: [],
    orden: null,
    created_at: null,
    updated_at: null
  };

  // File upload
  task: AngularFireUploadTask;
  percentage: Observable<number>;
  downloadURL: Observable<string>;
  snapshot: Observable<any>;
  isHovering: boolean;
  search: string;
  private publicidades$ = new BehaviorSubject(this.publicidadesService.getData());

  dataSource$: Observable<any[]>;
  columns = [
    'ver',
    // 'id',
    'nombre',
    'linkImagen',
    'orden',
    'created_at',
    'updated_at',
    'opciones'
  ];
  closeResult: string;
  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  pageSize = 10;


  private productos$ = new BehaviorSubject(this.productosService.getData());
  private categorias$ = new BehaviorSubject(this.categoriasService.getData());

  pDataSource$: Observable<any[]>;
  categoriasList: Observable<any[]>;
  pColumns = [
    'id',
    'nombre',
    // 'categoria_id',
    'precio',
    'disponible',
    'opciones'
  ];
  pDColumns = [
    'id',
    'nombre',
    // 'categoria_id',
    'precio',
    'disponible'
  ];
  pCurrentPage$ = new BehaviorSubject(1);
  pDataOnPage$: Observable<any[]>;


  constructor(
    private modalService: NgbModal,
    private publicidadesService: PublicidadesService,
    private productosService: ProductosService,
    private categoriasService: CategoriasService,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    // carga de datos de publicidades
    this.dataSource$ = this.publicidades$.value.pipe(map(v =>  Object.values(v)));
    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.publicidades$.value),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );

    // carga de datos de productos
    this.pDataSource$ = this.productos$.value.pipe(map(v =>  Object.values(v)));
    this.pDataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.productos$.value),
      map( v => {
        const idx = (this.pCurrentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
    this.categoriasList = this.categorias$.value.pipe(map(v =>  Object.values(v)));
  }

  open(content) {
    this.resetForm();
    this.loadData();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  editData(content, publicidad) {
    this.resetForm();
    this.loadData();
    this.publicidad = publicidad;
    //console.log(this.publicidad);
    let ref=this.storage.storage.refFromURL(this.publicidad.linkImagen);
    this.downloadURL = this.storage.ref(`${ref.fullPath}`).getDownloadURL();
    this.publicidad.detalle = publicidad.detalle.map(el => el.producto_id);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  deleteData(content, publicidad) {
    if (confirm('Are you sure you want to delete?')) {
      this.publicidadesService.deleteData(publicidad.id).subscribe((data)=>{
        let ref=this.storage.storage.refFromURL(publicidad.linkImagen);
        ref.delete();
        this.loadData();
        this.modalService.dismissAll();
      });
    }
    
  }

  onSubmit(publicidadForm) {
    if (this.publicidad.id === null) {
      this.publicidadesService.insertData(this.publicidad).subscribe((data)=>{
        this.loadData();
        this.resetForm();
        this.modalService.dismissAll();
      });
    } else {
      this.publicidadesService.updateData(this.publicidad).subscribe((data)=>{
        this.loadData();
        this.resetForm();
        this.modalService.dismissAll();
      });
    }

    this.snapshot=null;
    this.downloadURL=null;
    
  }

  resetForm() {
    this.publicidad = {
      id: null,
      nombre: null,
      linkImagen: null,
      detalle: [],
      orden: null,
      created_at: null,
      updated_at: null
    };
  }

  seleccionar(productoId) {
    const encontrado = this.publicidad.detalle.find( el => el === productoId);
    if (encontrado == null) {
      this.publicidad.detalle.push(productoId);
    } else {
      this.publicidad.detalle = this.publicidad.detalle.filter( el => el !== productoId);
    }
  }

  isInDetalle(prodId) {
    const encontrado = this.publicidad.detalle.find( el => el === prodId);
    return encontrado == null ? false : true;
  }


  toggleHover(event: boolean) {

  }

  deleteImage() {
    let ref=this.storage.storage.refFromURL(this.publicidad.linkImagen);
    ref.delete();
  }

  startUpload(event: FileList) {
    if(this.downloadURL){
      this.deleteImage();
    }
    const file = event.item(0);

    if (file.type.split('/')[0] !== 'image') {
      console.error('unsuported file');
      return;
    }

    const path = `publicidades/${new Date().getTime()}_${file.name}`;
    const customMetadata = { app: 'Pronto App'};
    this.task = this.storage.upload(path, file, { customMetadata });
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges();
    this.snapshot.pipe(finalize(() => {
      this.downloadURL = this.storage.ref(path).getDownloadURL()
      this.downloadURL.subscribe(res => this.publicidad.linkImagen = res)
    })).subscribe();
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  onKeyPress($event) {
    this.dataSource$ = this.publicidades$.value.pipe(map(el => Object.values(el).filter(v => {
      return v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search)
      || v.updated_at.includes(this.search) || v.orden.toUpperCase().includes(this.search.toUpperCase())
    })));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.publicidades$.value.pipe(
        map(el => Object.values(el)
        .filter(v => v.nombre.toUpperCase().includes(this.search) || v.created_at.includes(this.search)
        || v.updated_at.includes(this.search) || v.orden.toUpperCase().includes(this.search)
        )))
      ),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }

  productos(list, row) {
    const detalles = row.detalle.map(el => el.producto_id);
    this.pDataSource$ = this.productos$.value.pipe(map(el => Object.values(el).filter(v => detalles.includes(v.id))));
    this.pDataOnPage$ = this.pCurrentPage$.pipe(
      switchMap( () => this.productos$.value.pipe(
        map(el => Object.values(el).filter(v => detalles.includes(v.id))))),
        map( v => {
          const idx = (this.currentPage$.value - 1) * this.pageSize;
          return Object.values(v).slice(idx, idx + this.pageSize);
        })
      );
    this.modalService.open(list, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }
}
