import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { DeliveryComponent } from './delivery.component';
import { DeliveryRoutingModule } from './delivery-routing.module';
import { DeliveryServiceService } from '../../services/delivery-service.service';
import { AccordionGroupComponent } from '../../components/accordion/accordion-group.component';
import { AccordionComponent } from '../../components/accordion/accordion.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    CdkTableModule,
    DeliveryRoutingModule,
    NgbModule,
    FormsModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey:
      'AIzaSyAt7Mk0F78UZ5Oj33N0IPpZE8LgauIugYM'
      
    })
  ],
  declarations: [
   DeliveryComponent,
   AccordionGroupComponent , 
   AccordionComponent
   ],
  exports: [],
  providers: [
    DeliveryServiceService
  ]
})
export class DeliveryModule { }
