import { Component, OnInit,NgZone } from '@angular/core';
import { RolesService } from '../../services/roles.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { DeliveryServiceService } from '../../services/delivery-service.service';
import { ProductosService } from '../../services/productos.service';
import { switchMap, map } from 'rxjs/operators';
import { UsersService } from '../../services/users.service';
import { NgForm } from '@angular/forms';
import { MessagingService } from '../../shared/messaging.service';
import { HostListener } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {

  private pedidos$ = new BehaviorSubject(this.deliverSerivce.getData());
  private productos$ = new BehaviorSubject(this.productosService.getData());

  private repartidores$ = new BehaviorSubject(this.deliverSerivce.getRepartidores());



  role = new BehaviorSubject(this.roleService.getRole());
  currentPage$ = new BehaviorSubject(1);
  currentPage2$ = new BehaviorSubject(1);
  currentPage3$ = new BehaviorSubject(1);
  currentPage4$ = new BehaviorSubject(1);
  currentPage5$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  dataOnPage2$: Observable<any[]>;
  dataOnPage3$: Observable<any[]>;
  dataOnPage4$: Observable<any[]>;
  dataOnPage5$: Observable<any[]>;
  dataOnPageRepartidores$: Observable<any[]>;
  pageSize = 30;
  pageSize2 = 10;
  productosList: Observable<any[]>;
  usuariosList: Observable<any[]>;

  orden_id:number;
  dataSource2$;
  dataSource3$;

  data={
    time:null,
    ordenId:null

  }

  iconMap={
    url:"../../assets/img/pin.svg",
    scaledSize: {
      width: 30,
      height:30

    }
  }

  user = {
    id: null,
    direccion: null,
    lat:null,
    lng:null
  };
  pedidosEstab:any;
  zoomMap:number=13;
  isOpen:boolean=true;
  @HostListener('window:focus', ['$event'])
   onFocus(event: FocusEvent): void {
       this.loadData(); 
   }

  constructor(  
    private modalService: NgbModal,  
    private zone:NgZone,
    private roleService: RolesService,
    private deliverSerivce:DeliveryServiceService,
    private productosService: ProductosService,
    private usersService:UsersService,
    private messagingService: MessagingService
  ) { 
    
    this.messagingService.receiveMessage().subscribe((payload)=>{
      this.loadData();
      this.zone.run(() => { // <== added
      });
    });
    
  }

  resetForm() {
    this.user = {
      id: null,
      direccion: null,
      // calificacion: null,
      lat:-2.1401497848132904,
      lng:-79.59549072846681
    };
    this.pedidosEstab=null;
  }

  open(content,item:any) {
    // this.establecimiento.lat=-2.1406216;
    // this.establecimiento.lng=-79.6257032;
    this. resetForm();
    this.zoomMap=13;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
    this.user.lat=parseFloat(item.latEnvio);
    this.user.lng=parseFloat(item.lngEnvio);
    this.user.direccion=item.direccionEnvio;
    this.pedidosEstab=item.pedidos;
    this.pedidosEstab.latEstab=parseFloat(this.pedidosEstab.latEstab);
    this.pedidosEstab.lngEstab=parseFloat(this.pedidosEstab.lngEstab);
    
  }

  ngOnInit() {

    this.loadData();
  }

  loadData() {
    
    this.productosList = this.productos$.value.pipe(map(v =>  Object.values(v)));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.pedidos$.value),
      map( (v:any) => {
        let delivery:any[]=v.filter(function(del) {
          if(del.estado){
            return del.estado == "DR" || del.estado == "DRCA" || del.estado == "DA";
          }
          if(del.estate){
            return del.estate == "DR" || del.estate == "DRCA" || del.estate == "DA";

          }
        });
        
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(delivery).slice(idx, idx + this.pageSize);
      })
    );
    this.dataOnPage2$ = this.currentPage3$.pipe(
      switchMap( () => this.pedidos$.value),
      map( (v:any) => {

        let delivery:any[]=v.filter(function(del) {
          if(del.estado){
            return del.estado == "procesando" || del.estado == "TS"  || (del.repartidor == null && del.estado == "DR");
          }
          if(del.estate){
            return del.estate == "procesando" || del.estate == "TS"  || (del.estate == null && del.estate == "DR");
          }
          
        });

        const idx = (this.currentPage3$.value - 1) * this.pageSize;
        return Object.values(delivery).slice(idx, idx + this.pageSize);
      })
    );
    this.dataOnPage3$ = this.currentPage4$.pipe(
      switchMap( () => this.pedidos$.value),
      map( (v:any) => {

        let delivery:any[]=v.filter(function(del) {
          if(del.estado){
            return del.estado == "completo";

          }
          if(del.estate){
            return del.estate == "completo";

          }
        });
        this.dataSource3$ = delivery.length;

        delivery=this.getsortData(delivery);

        const idx = (this.currentPage4$.value - 1) * this.pageSize2;
        return Object.values(delivery).slice(idx, idx + this.pageSize2);
      })
    );

    this.dataOnPage4$ = this.currentPage5$.pipe(
      switchMap( () => this.pedidos$.value),
      map( (v:any) => {

        let delivery:any[]=v.filter(function(del) {
          if(del.estado){
            return del.estado == "cancelado";
          }
          if(del.estate){
            return del.estate == "cancelado";
          }
        });
        
        this.dataSource2$ = delivery.length;
        delivery=this.getsortData(delivery);

        
        const idx = (this.currentPage5$.value - 1) * this.pageSize2;
        return Object.values(delivery).slice(idx, idx + this.pageSize2);
      })
    );
    this.dataOnPageRepartidores$ = this.currentPage2$.pipe(
      switchMap( () => this.repartidores$.value),
      map( v => {
        const idx = (this.currentPage2$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }
  

  selectTime(time,ordenId){
    this.data.time=time;
    this.data.ordenId=ordenId;
    this.deliverSerivce.selectTime(this.data).subscribe((data)=>{
      this.loadData();
    });

  }

  onSubmit(form:NgForm,ordenId){
    var delRep=parseInt(form.value.delRep);
    var data={
      delPrice: form.value.delPrice,
      delRep: delRep,
      time: form.value.time,
      ordenId:ordenId
    }
    
    
    this.deliverSerivce.setTimePriceDelOrder(data).subscribe((data)=>{
      this.loadData();
    });
    
    
  }

  getsortData(array:any[]) {
    return array.sort((a, b) => {
      return <any>new Date(b.orden_actualizada) - <any>new Date(a.orden_actualizada);
    });
  }

  setRepartidor(form:NgForm,ordenId){
    var delRep=parseInt(form.value.delRep);
    var data={
      delRep: delRep,
      time: form.value.time,
      ordenId:ordenId
    }
    
    this.deliverSerivce.setRepOnly(data).subscribe((data)=>{
      this.loadData();
    });
    
    
  }

}
