import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ModalService } from '../../components/modal';
import { MessagingService } from '../../shared/messaging.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from '../../services/token.service';
@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  user = null;
  token:string;
  constructor(private auth: AuthService,
    private modalService: ModalService,
    private messagingService:MessagingService,
    private authservice:AuthService,
    private activeroute:ActivatedRoute,
    private router:Router,
    private tokenService:TokenService
    ) {
    this.user = auth.getUser();
    if(this.router.getCurrentNavigation().extras.state){
      this.token=this.router.getCurrentNavigation().extras.state.token;

    }
    else{
      this.token=this.tokenService.get();
    }

  }

  bodyText: string;

  ngOnInit() {
    this.bodyText = 'This text can be updated in modal 1';
    
    
    
    this.activeroute.data.subscribe(params=>{
      
    });
  }

  ngAfterViewInit(){
    if(!this.authservice.getFireToken()){
      this.openModal("custom-modal-1");
    }
  }

  openModal(id: string) {
      this.modalService.open(id);
  }

  closeModal(id: string) {
      this.modalService.close(id);
  }

  requestAuthorization(){
    try{
      this.messagingService.checkPermissions()
      .subscribe((data) => 
      { 
       (data);
        this.messagingService.getToken()
        .subscribe((data)=>{
          this.authservice.registerFirebaseToken(this.user,this.token,data);
          this.closeModal("custom-modal-1");
        });
        
        
        // this.messagingService.requestPermission(); 
      }
      ,
      (error) => { 
        
        this.openModal("custom-modal-1");
        //(error);
        // this.requestAuthorization();
        // this.messagingService.requestPermission();   
      },  
      );
    }
    catch (error) {
      console.error('Here is the error message', error);
      // this.messagingService.checkPermissions();
    }

    
    
  }
}
