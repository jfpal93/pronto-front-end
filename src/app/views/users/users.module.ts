import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UsersService } from '../../services/users.service';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    UsersComponent
   ],
  exports: [],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
