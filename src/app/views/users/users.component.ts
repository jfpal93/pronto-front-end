import { Component, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, tap, finalize } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../services/auth.service';
import { UsersService } from '../../services/users.service';
import { RolesService } from '../../services/roles.service';
import { EstablecimientosService } from '../../services/establecimientos.service';
import { AngularFireUploadTask, AngularFireStorage } from 'angularfire2/storage';
import { HttpHeaders } from '@angular/common/http';
import { TokenService } from '../../services/token.service';
// import { RolesService } from '../../services/roles.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  // File upload
  task: AngularFireUploadTask;
  percentage: Observable<number>;
  downloadURL: Observable<string>;
  snapshot: Observable<any>;
  isHovering: boolean;

  public usuario = {
    id: null,
    email: null,
    name: null,
    direccion: null,
    telefono: null,
    password: null,
    // email_verified_at: null,
    // created_at: null,
    linkImagen: null,
    roles: [{}],
    establecimiento_id: null,
    // updated_at: null
  };

  headers = new HttpHeaders({
    'Accept': 'application/json;charset=utf-8',
  'Content-Type': 'application/json;charset=utf-8',
    'Authorization': `Bearer ${this.token.get()}`
  });

  private usuarios$ = new BehaviorSubject(this.usersService.getData());
  private roles$ = new BehaviorSubject(this.rolesService.getData(this.headers));
  private establecimientos$ = new BehaviorSubject(this.establecimientosService.getData());

  dataSource$: Observable<any[]>;
  rolesList: Observable<any[]>;
  establecimientosList: Observable<any[]>;

  columns = [
    // 'id',
    'name',
    'telefono',
    'direccion',
    'email',
    'establecimiento_id',
    'roles',
    'opciones'
  ];

  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  pageSize = 10;
  constructor(
    private modalService: NgbModal,
    private authService: AuthService,
    private usersService: UsersService,
    private rolesService: RolesService,
    private establecimientosService: EstablecimientosService,
    private storage: AngularFireStorage,
    private token: TokenService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.rolesList = this.roles$.value.pipe(map(v =>  Object.values(v)));
    this.dataSource$ = this.usuarios$.value.pipe(map(v =>  Object.values(v)));
    this.establecimientosList = this.establecimientos$.value.pipe(map(v =>  Object.values(v)));
    
    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.usuarios$.value),
      map( v => {        
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
    
  }
  onSubmit() {
    if (this.usuario.id === null) {
      this.authService.register(this.usuario).subscribe(
        data => {
          this.loadData();
        },
        error => this.handleError(error)
      );
    } else {
      this.usersService.updateData(this.usuario).subscribe(data=>{
        this.loadData();
      });
    }
    
    this.resetForm();
    this.modalService.dismissAll();
    this.snapshot=null;
    this.downloadURL=null;
    
  }

  handleError(error) {

  }

  editData(content, usuario) {
    this.snapshot=null;
    this.downloadURL=null;
    this. resetForm();
    this.usuario = usuario;
    let ref=this.storage.storage.refFromURL(this.usuario.linkImagen);
    this.downloadURL = this.storage.ref(`${ref.fullPath}`).getDownloadURL();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  deleteImage() {
    let ref=this.storage.storage.refFromURL(this.usuario.linkImagen);
    ref.delete();
  }

  deleteData(content, usuario) {
    //console.log(usuario);
    
    if (confirm('¿Está seguro de eliminar este item?')) {
      this.usersService.deleteData(usuario.id).subscribe(data=>{
        this.loadData();
      });
      let ref=this.storage.storage.refFromURL(usuario.linkImagen);
      ref.delete();
    }
    this. resetForm();
    
    this.modalService.dismissAll();
    // this.loadData();
  }

  open(content) {
    this. resetForm();
    this.snapshot=null;
    this.downloadURL=null;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  resetForm() {
    this.usuario = {
      id: null,
      email: null,
      name: null,
      direccion: null,
      telefono: null,
      password: null,
      // email_verified_at: null,
      // created_at: null,
      linkImagen: null,
      establecimiento_id: null,
      roles: [{}],
      // updated_at: null
    };
  }

  functionFilter(row){

    if(row.establecimiento_id != null){
      return true;
    }

    return false;

    
  }

  showPronto(row){
    if(row.establecimiento_id == null){
      return true;
    }

    return false;

  }

  startUpload(event: FileList) {
    if(this.downloadURL){
      this.deleteImage();
    }
    
    const file = event.item(0);

    if (file.type.split('/')[0] !== 'image') {
      return;
    }

    const path = `users/${new Date().getTime()}_${file.name}`;
    
    const customMetadata = { app: 'Pronto App'};
    this.task = this.storage.upload(path, file, { customMetadata });
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges();
    this.snapshot.pipe(finalize(() => {
      this.downloadURL = this.storage.ref(path).getDownloadURL();
      this.downloadURL.subscribe(res => this.usuario.linkImagen = res);
    })).subscribe();
  }

  toggleHover(event: boolean) {

  }
}
