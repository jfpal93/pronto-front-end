import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {FavoritosRoutingModule } from './favoritos-routing.module';
import {FavoritosComponent } from './favoritos.component';
import {FavoritosService } from '../../services/favoritos.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FavoritosRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
   FavoritosComponent
   ],
  exports: [],
  providers: [
   FavoritosService
  ]
})
export class FavoritosModule { }
