import { Component, OnInit } from '@angular/core';
import { FavoritosService } from '../../services/favoritos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CategoriasService } from '../../services/categorias.service';
import { ProductosService } from '../../services/productos.service';

@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.scss']
})
export class FavoritosComponent implements OnInit {

  favorito = {
    id: null,
    nombre: null,
    detalle: [],
    created_at: null,
    updated_at: null
  };


  dataSource$: Observable<any[]>;
  columns = [
    'ver',
    // 'id',
    'nombre',
    // 'created_at',
    // 'updated_at',
    'opciones'
  ];
  closeResult: string;
  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  pageSize = 10;
  search: string;

  private favoritos$ = new BehaviorSubject(this.favoritosService.getData());
  private productos$ = new BehaviorSubject(this.productosService.getData());
  private categorias$ = new BehaviorSubject(this.categoriasService.getData());

  pDataSource$: Observable<any[]>;
  categoriasList: Observable<any[]>;
  pColumns = [
    'id',
    'nombre',
    // 'categoria_id',
    'precio',
    'disponible',
    'opciones'
  ];
  pDColumns = [
    'id',
    'nombre',
    // 'categoria_id',
    'precio',
    'disponible'
  ];
  pCurrentPage$ = new BehaviorSubject(1);
  pDataOnPage$: Observable<any[]>;


  constructor(
    private modalService: NgbModal,
    private favoritosService: FavoritosService,
    private productosService: ProductosService,
    private categoriasService: CategoriasService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {    
    this.categoriasList = this.categorias$.value.pipe(map(v =>  Object.values(v)));

    this.dataSource$ = this.favoritos$.value.pipe(map(v =>  Object.values(v)));
    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.favoritos$.value),
      map( v => {
        //console.log(v);
        
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );

    // carga de datos de productos
    this.pDataSource$ = this.productos$.value.pipe(map(v =>  Object.values(v)));
    this.pDataOnPage$ = this.pCurrentPage$.pipe(
      switchMap( () => this.productos$.value),
      map( v => {
        //console.log(v);
        
        const idx = (this.pCurrentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );

  }

  open(content) {
    this.resetForm();
    this.loadData();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  editData(content, favorito) {
    this.resetForm();
    this.loadData();
    this.favorito = favorito;
    //console.log(this.favorito.detalle);
    this.favorito.detalle = favorito.detalle.map(el => el.producto_id);
    //console.log(this.favorito.detalle);
    
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  deleteData(content, favorito) {
    if (confirm('Are you sure you want to delete?')) {
      this.favoritosService.deleteData(favorito.id).subscribe();
    }
    this.loadData();
    this.modalService.dismissAll();
  }

  onSubmit(favoritoForm) {
    if (this.favorito.id === null) {
      this.favoritosService.insertData(this.favorito).subscribe((data)=>{
        this.loadData();
        this.resetForm();
        this.modalService.dismissAll();
        //console.log(data);
      });
    } else {
      //console.log(this.favorito);
      
      this.favoritosService.updateData(this.favorito).subscribe((data)=>{
        this.loadData();
        this.resetForm();
        this.modalService.dismissAll();
        //console.log(data);
        
      });
    }
    
  }

  resetForm() {
    this.favorito = {
      id: null,
      nombre: null,
      detalle: [],
      created_at: null,
      updated_at: null
    };
  }


  seleccionar(productoId) {
    const encontrado = this.favorito.detalle.find( el => el === productoId);
    //console.log(encontrado);
    
    if(encontrado == null) {
      this.favorito.detalle.push(productoId);
      //console.log(this.favorito.detalle);
    } else {
      this.favorito.detalle = this.favorito.detalle.filter( el => el !== productoId);
      //console.log(this.favorito.detalle);
    }
    
  }

  isInDetalle(prodId) {
    const encontrado=this.favorito.detalle
    .find( el => el === prodId);
    // .forEach((dat)=>{
    //   //console.log(dat);
      
    // })

    // //console.log(this.favorito);
    
    
    
    // //console.log(prodId);
    // //console.log(encontrado);
    
    return encontrado == null ? false : true;
    // return false;

    // for(let det of this.favorito.detalle){
    //   //console.log(det);
    //   if(det === prodId){
    //     return true;
    //   }
      
    // }
    // return false;
  }

  onKeyPress($event) {
    this.dataSource$ = this.favoritos$.value.pipe(map(el => Object.values(el).filter(v => {
      return v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search)
            || v.updated_at.includes(this.search);
    })));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.favoritos$.value.pipe(
        map(el => Object.values(el)
        .filter(v => v.nombre.toUpperCase().includes(this.search.toUpperCase())
            || v.created_at.includes(this.search) || v.updated_at.includes(this.search)))
        )
      ),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }

  productosFavoritos(list, row) {
    const favoritos = row.detalle.map(el => el.producto_id);
    this.pDataSource$ = this.productos$.value.pipe(map(el => Object.values(el).filter(v => favoritos.includes(v.id))));

    this.pDataOnPage$ = this.pCurrentPage$.pipe(
      switchMap( () => this.productos$.value.pipe(
        map(el => Object.values(el).filter(v => favoritos.includes(v.id))))),
        map( v => {
          const idx = (this.currentPage$.value - 1) * this.pageSize;
          return Object.values(v).slice(idx, idx + this.pageSize);
        })
      );
      this.modalService.open(list, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});

  }
}
