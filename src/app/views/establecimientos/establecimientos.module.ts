import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {EstablecimientosRoutingModule } from './establecimientos-routing.module';
import {EstablecimientosComponent } from './establecimientos.component';
import {EstablecimientosService } from '../../services/establecimientos.service';
import { SharedModule } from '../../shared/shared.module';
// import { UsersService } from '../../services/users.service';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    EstablecimientosRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey:
      'AIzaSyAt7Mk0F78UZ5Oj33N0IPpZE8LgauIugYM'
      
    })
  ],
  declarations: [
   EstablecimientosComponent
   ],
  exports: [],
  providers: [
   EstablecimientosService
  ]
})
export class EstablecimientosModule { }
