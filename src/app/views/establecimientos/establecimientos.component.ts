import { Component, OnInit, ViewChild } from '@angular/core';
import { EstablecimientosService } from '../../services/establecimientos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, finalize } from 'rxjs/operators';
import { AngularFireUploadTask, AngularFireStorage } from 'angularfire2/storage';
import { CategoriasService } from '../../services/categorias.service';
import { AgmMap } from '@agm/core';

@Component({
  selector: 'app-establecimientos',
  templateUrl: './establecimientos.component.html',
  styleUrls: ['./establecimientos.component.scss']
})
export class EstablecimientosComponent implements OnInit {

  // latt=0.00;
  // lat
  lat:number=-2.1401497848132904;
  lng:number=-79.59549072846681;
  establecimiento = {
    id: null,
    nombre: null,
    categoria_id: null,
    linkImagen: null,
    direccion: null,
    // calificacion: null,
    lat:null,
    lng:null,
    // created_at: null,
    // updated_at: null
  };

  zoomMap:number=13;
  isOpen:boolean=true;

  // File upload
  task: AngularFireUploadTask;
  percentage: Observable<number>;
  downloadURL: Observable<string>;
  snapshot: Observable<any>;
  isHovering: boolean;
  search: string;
  private categorias$ = new BehaviorSubject(this.categoriasService.getData());
  private establecimientos$ = new BehaviorSubject(this.establecimientosService.getData());

  dataSource$: Observable<any[]>;
  columns = [
    // 'id',
    'nombre',
    'categoria_id',
    'linkImagen',
    'direccion',
    // 'calificacion',
    // 'created_at',
    // 'updated_at',
    'opciones'
  ];
  closeResult: string;
  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  categoriasList: Observable<any[]>;
  pageSize = 10;

  @ViewChild(AgmMap,{static:false}) public agmMap: AgmMap;

  constructor(
    private modalService: NgbModal,
    private establecimientosService: EstablecimientosService,
    private storage: AngularFireStorage,
    private categoriasService: CategoriasService
  ) { }

  ngOnInit() {
    this.loadData();
    
    
    // this.establecimientosService.getPosition().then(pos=>{
    //   this.establecimiento.lat=pos.lat;
    //   this.establecimiento.lng=pos.lng;
    // });
    
  }

  loadData() {
    this.dataSource$ = this.establecimientos$.value.pipe(map(v =>  Object.values(v)));
    
    this.categoriasList = this.categorias$.value.pipe(map(v =>  Object.values(v)));
    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.establecimientos$.value),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }

  open(content) {
    // this.establecimiento.lat=-2.1406216;
    // this.establecimiento.lng=-79.6257032;
    this. resetForm();
    this.snapshot=null;
    this.downloadURL=null;
    this.zoomMap=13;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  editData(content, establecimiento) {
    this. resetForm();
    this.establecimiento = establecimiento;
    this.establecimiento.lat=parseFloat(establecimiento.lat);
    this.establecimiento.lng=parseFloat(establecimiento.lng);
    // this.lat=parseFloat(establecimiento.lat);
    // this.lng=parseFloat(establecimiento.lng);
    this.zoomMap=17;
    this.isOpen=false;  
    let ref=this.storage.storage.refFromURL(this.establecimiento.linkImagen);

    this.downloadURL = this.storage.ref(`${ref.fullPath}`).getDownloadURL();

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }
  deleteData(content, establecimiento) {
    if (confirm('¿Está seguro de eliminar este item?')) {
      this.establecimientosService.deleteData(establecimiento.id).subscribe(data=>{
        this.loadData();
        let ref=this.storage.storage.refFromURL(establecimiento.linkImagen);
        ref.delete();
      });
    }
    this. resetForm();
    this.modalService.dismissAll();
  }

  onSubmit(establecimientoForm) {
    if (this.establecimiento.id === null) {
      this.establecimientosService.insertData(this.establecimiento).subscribe(data=>{
        this.loadData();
      });
    } else {
      this.establecimientosService.updateData(this.establecimiento).subscribe(data=>{
        this.loadData();
      });
    }
    this.resetForm();
    this.modalService.dismissAll();
    this.snapshot=null;
    this.downloadURL=null;
  }

  toggleHover(event: boolean) {

  }

  resetForm() {
    this.establecimiento = {
      id: null,
      nombre: null,
      categoria_id: null,
      direccion: null,
      // calificacion: null,
      lat:-2.1401497848132904,
      lng:-79.59549072846681,
      linkImagen: null,
      // created_at: null,
      // updated_at: null
    };
  }

  startUpload(event: FileList) {
    if(this.downloadURL){
      this.deleteImage();
    }
    const file = event.item(0);

    if (file.type.split('/')[0] !== 'image') {
      return;
    }

    const path = `establecimientos/${new Date().getTime()}_${file.name}`;
    const customMetadata = { app: 'Pronto App'};
    this.task = this.storage.upload(path, file, { customMetadata });
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges();
    this.snapshot.pipe(finalize(() => {
      this.downloadURL = this.storage.ref(path).getDownloadURL();
      this.downloadURL.subscribe(res => this.establecimiento.linkImagen = res);
    })).subscribe();
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  onKeyPress($event) {
    this.dataSource$ = this.establecimientos$.value.pipe(map(el => Object.values(el).filter(v => {
      return v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search)
              || v.updated_at.includes(this.search);
    })));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.establecimientos$.value.pipe(
        map(el => Object.values(el)
        .filter(v => v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search)
                || v.updated_at.includes(this.search)))
        )
      ),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }

  markerDragEnd(evnt:any){
    this.lat=evnt.coords.lat;
    this.lng=evnt.coords.lng;

    this.establecimiento.lat=evnt.coords.lat;
    this.establecimiento.lng=evnt.coords.lng;
  }

  click(evnt:any){
    this.lat=evnt.coords.lat;
    this.lng=evnt.coords.lng;
    
    this.establecimiento.lat=evnt.coords.lat;
    this.establecimiento.lng=evnt.coords.lng;
  }

  deleteImage() {
    let ref=this.storage.storage.refFromURL(this.establecimiento.linkImagen);
    ref.delete();
  }

  
}
