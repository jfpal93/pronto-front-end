import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstablecimientosComponent } from './establecimientos.component';

const routes: Routes = [
  {
    path: '',
    component: EstablecimientosComponent,
    data: {
      title: 'Establecimientos'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstablecimientosRoutingModule {}
