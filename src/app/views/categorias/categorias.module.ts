import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {CategoriasRoutingModule } from './categorias-routing.module';
import {CategoriasComponent } from './categorias.component';
import {CategoriasService } from '../../services/categorias.service';
import { SharedModule } from '../../shared/shared.module';
// import { UsersService } from '../../services/users.service';
@NgModule({
  imports: [
    CommonModule,
    CategoriasRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule,
    SharedModule,
  ],
  declarations: [
   CategoriasComponent
   ],
  exports: [],
  providers: [
   CategoriasService
  ]
})
export class CategoriasModule { }
