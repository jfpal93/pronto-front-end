import { Component, OnInit } from '@angular/core';
import { CategoriasService } from '../../services/categorias.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, switchMap, finalize, filter } from 'rxjs/operators';
import { AngularFireUploadTask, AngularFireStorage } from 'angularfire2/storage';


@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {

  categoria = {
    id: null,
    nombre: null,
    linkImagen: null,
    // linkIcono: null,
    // created_at: null,
    // updated_at: null
  };

  // File upload
  task: AngularFireUploadTask;
  percentage: Observable<number>;
  downloadURL: Observable<string>;
  snapshot: Observable<any>;
  isHovering: boolean;

  // icon upload
  // taskIco: AngularFireUploadTask;
  // percentageIco: Observable<number>;
  // downloadURLIco: Observable<string>;
  // snapshotIco: Observable<any>;
  // isHoveringIco: boolean;

  private categorias$ = new BehaviorSubject(this.categoriasService.getData());

  dataSource$: Observable<any[]>;
  columns = [
    // 'id',
    'nombre',
    'linkImagen',
    // 'created_at',
    // 'updated_at',
    'opciones'
  ];
  search: string;
  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  pageSize = 10;

  constructor(
    private modalService: NgbModal,
    private categoriasService: CategoriasService,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.dataSource$ = this.categorias$.value.pipe(map(v => Object.values(v)));
    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.categorias$.value),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }

  open(content) {
    this. resetForm();
    this.snapshot=null;
    this.downloadURL=null;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  editData(content, categoria) {
    this. resetForm();
    this.categoria = categoria;
    let ref=this.storage.storage.refFromURL(this.categoria.linkImagen);
    this.downloadURL = this.storage.ref(`${ref.fullPath}`).getDownloadURL();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  deleteData(content, categoria) {
    if (confirm('¿Está seguro de eliminar este item?')) {
      this.categoriasService.deleteData(categoria.id).subscribe(data=>{
        this.loadData();
        let ref=this.storage.storage.refFromURL(categoria.linkImagen);
        ref.delete();
        this. resetForm();
        this.modalService.dismissAll();
      });
    }
    
  }

  deleteImage() {
    let ref=this.storage.storage.refFromURL(this.categoria.linkImagen);
    ref.delete();
  }

  onSubmit(categoriaForm) {
    if (this.categoria.id === null) {
      this.categoriasService.insertData(this.categoria).subscribe(data=>{
        this.loadData();
      });
    } else {
      this.categoriasService.updateData(this.categoria).subscribe(data=>{
        this.loadData();
      });
    }
    this.resetForm();
    this.modalService.dismissAll();
    this.snapshot=null;
    this.downloadURL=null;
  }

  resetForm() {
    this.categoria = {
      id: null,
      nombre: null,
      linkImagen: null,
      // linkIcono: null,
      // created_at: null,
      // updated_at: null
    };
  }

  onKeyPress($event) {
    this.dataSource$ = this.categorias$.value.pipe(map(el => Object.values(el).filter(v => {
      return v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search)
              || v.updated_at.includes(this.search);
    })));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.categorias$.value.pipe(
        map(el => Object.values(el)
        .filter(v => v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search)
              || v.updated_at.includes(this.search)))
        )
      ),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }

  startUpload(event: FileList) {
    if(this.downloadURL){
      this.deleteImage();
    }
    const file = event.item(0);

    if (file.type.split('/')[0] !== 'image') {
      return;
    }

    const path = `categorias/${new Date().getTime()}_${file.name}`;
    const customMetadata = { app: 'Pronto App'};
    this.task = this.storage.upload(path, file, { customMetadata });
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges();
    this.snapshot.pipe(finalize(() => {
      this.downloadURL = this.storage.ref(path).getDownloadURL();
      this.downloadURL.subscribe(res => this.categoria.linkImagen = res);
    })).subscribe();
  }

  // startUploadIco(event: FileList) {
  //   const file = event.item(0);

  //   if (file.type.split('/')[0] !== 'image') {
  //     console.error('unsuported file');
  //     return;
  //   }

  //   const path = `categoriasIcon/${new Date().getTime()}_${file.name}`;
  //   const customMetadata = { app: 'Pronto App'};
  //   this.taskIco = this.storage.upload(path, file, { customMetadata });
  //   this.percentageIco = this.taskIco.percentageChanges();
  //   this.snapshotIco = this.taskIco.snapshotChanges();
  //   this.snapshotIco.pipe(finalize(() => {
  //     this.downloadURLIco = this.storage.ref(path).getDownloadURL();
  //     this.downloadURLIco.subscribe(res => this.categoria.linkIcono = res);
  //   })).subscribe();
  // }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  toggleHover(event: boolean) {

  }
}
