import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { TokenService } from '../../services/token.service';
import { Router } from '@angular/router';
import { RolesService } from '../../services/roles.service';
import { EstablecimientosService } from '../../services/establecimientos.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  public form = {
    email: null,
    password: null
  };
  constructor(
    private authService: AuthService,
    private tokenService: TokenService,
    private rolesService: RolesService,
    private establecimientoService: EstablecimientosService,
    private router: Router
    ) {

  }

  onSubmit() {
    return this.authService.login(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data) {
    
    this.authService.handleUser(data.user);
    this.rolesService.handle(data.user.roles[0].name);
    this.establecimientoService.handle( (data.user.establecimiento !== null ? data.user.establecimiento.id : null) );
    this.tokenService.handle(data.access_token);
    
    this.router.navigateByUrl('/dashboard',{state:{token:data.access_token}});
  }

  handleError(error) {
    this.router.navigateByUrl('/login');
  }
}
