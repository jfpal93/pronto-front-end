import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../../services/productos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, finalize } from 'rxjs/operators';
import { CategoriasService } from '../../services/categorias.service';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { EstablecimientosService } from '../../services/establecimientos.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  producto = {
    id: null,
    nombre: null,
    // categoria_id: null,
    establecimiento_id: null,
    // descripcion: null,
    linkImagen: null,
    precio: null,
    disponible: false,
    created_at: null,
    updated_at: null
  };

  // File upload
  task: AngularFireUploadTask;
  percentage: Observable<number>;
  downloadURL: Observable<string>;
  snapshot: Observable<any>;
  isHovering: boolean;

  private productos$ = new BehaviorSubject(this.productosService.getData());
  // private categorias$ = new BehaviorSubject(this.categoriasService.getData());
  private establecimientos$ = new BehaviorSubject(this.establecimientosService.getData());

  dataSource$: Observable<any[]>;
  // categoriasList: Observable<any[]>;
  establecimientosList: Observable<any[]>;
  columns = [
    'id',
    'nombre',
    // 'categoria_id',
    // 'establecimiento_id',
    // 'descripcion',
    'linkImagen',
    'precio',
    'disponible',
    'created_at',
    'updated_at',
    'opciones'
  ];
  closeResult: string;
  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  pageSize = 10;
  search: string;

  constructor(
    private modalService: NgbModal,
    private productosService: ProductosService,
    // private categoriasService: CategoriasService,
    private establecimientosService: EstablecimientosService,
    private storage: AngularFireStorage
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.dataSource$ = this.productos$.value.pipe(map(v =>  Object.values(v)));
    // this.categoriasList = this.categorias$.value.pipe(map(v =>  Object.values(v)));
    this.establecimientosList = this.establecimientos$.value.pipe(map(v =>  Object.values(v)));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.productos$.value),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  editData(content, producto) {
    this. resetForm();
    this.producto = producto;
    let ref=this.storage.storage.refFromURL(this.producto.linkImagen);
    this.downloadURL = this.storage.ref(`${ref.fullPath}`).getDownloadURL();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }
  deleteData(content, producto) {
    if (confirm('Are you sure you want to delete?')) {
      this.productosService.deleteData(producto.id).subscribe((data)=>{
        this.loadData();
        // let ref=this.storage.storage.refFromURL(producto.linkImagen);
        // ref.delete();
        this.resetForm();
        this.modalService.dismissAll();
      });
    }
    
  }

  onSubmit(productoForm) {
    this.producto.establecimiento_id = this.establecimientosService.getEstablecimiento();
    if (this.producto.id === null) {
      this.productosService.insertData(this.producto).subscribe((data)=>{
        this.loadData();
        this.resetForm();
        this.modalService.dismissAll();
      });
    } else {
      this.productosService.updateData(this.producto).subscribe((data)=>{
        this.loadData();
        this.resetForm();
        this.modalService.dismissAll();
      });
    }
    this.snapshot=null;
    this.downloadURL=null;
    
  }

  resetForm() {
    this.downloadURL = null;
    this.producto = {
      id: null,
      nombre: null,
      // categoria_id: null,
      establecimiento_id: null,
      // descripcion: null,
      linkImagen: null,
      precio: null,
      disponible: false,
      created_at: null,
      updated_at: null
    };
  }

  toggleHover(event: boolean) {

  }

  deleteImage() {
    let ref=this.storage.storage.refFromURL(this.producto.linkImagen);
    ref.delete();
  }

  startUpload(event: FileList) {
    if(this.downloadURL){
      this.deleteImage();
    }
    const file = event.item(0);

    if (file.type.split('/')[0] !== 'image') {
      console.error('unsuported file');
      return;
    }

    const path = `productos/${new Date().getTime()}_${file.name}`;
    const customMetadata = { app: 'Pronto App'};
    this.task = this.storage.upload(path, file, { customMetadata });
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges();
    this.snapshot.pipe(finalize(() => {
      this.downloadURL = this.storage.ref(path).getDownloadURL();
      this.downloadURL.subscribe(res => this.producto.linkImagen = res);
    })).subscribe();
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  onKeyPress($event) {
    this.dataSource$ = this.productos$.value.pipe(map(el => Object.values(el).filter(v => {
      return v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search)
          || v.updated_at.includes(this.search) || String(v.precio).includes(this.search)
          || String(v.disponible).includes(this.search);
    })));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.productos$.value.pipe(
        map(el => Object.values(el)
        .filter(v => v.nombre.toUpperCase().includes(this.search.toUpperCase()) || v.created_at.includes(this.search) ||
                    v.updated_at.includes(this.search) || String(v.precio).includes(this.search) ||
                    String(v.disponible).includes(this.search)
        )))
      ),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }
}
