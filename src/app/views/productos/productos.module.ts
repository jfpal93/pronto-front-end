import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {ProductosRoutingModule } from './productos-routing.module';
import {ProductosComponent } from './productos.component';
import {ProductosService } from '../../services/productos.service';
import { SharedModule } from '../../shared/shared.module';

// import { UsersService } from '../../services/users.service';
@NgModule({
  imports: [
    CommonModule,
    ProductosRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
   ProductosComponent,
   ],
  exports: [],
  providers: [
   ProductosService
  ]
})
export class ProductosModule { }
