import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SugerenciasRoutingModule } from './sugerencias-routing.module';
import { SugerenciasComponent } from './sugerencias.component';
import { SugerenciasService } from '../../services/sugerencias.service';
import { ObservableFilterPipe } from '../../pipes/observable-filter.pipe';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SugerenciasRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
   SugerenciasComponent,
   ],
  exports: [],
  providers: [
   SugerenciasService
  ]
})
export class SugerenciasModule { }
