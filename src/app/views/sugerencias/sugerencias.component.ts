import { Component, OnInit } from '@angular/core';
import { SugerenciasService } from '../../services/sugerencias.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ProductosService } from '../../services/productos.service';
import { CategoriasService } from '../../services/categorias.service';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-sugerencias',
  templateUrl: './sugerencias.component.html',
  styleUrls: ['./sugerencias.component.scss']
})
export class SugerenciasComponent implements OnInit {

  sugerencia = {
    id: null,
    categoria_id: null,
    detalle: [],
    created_at: null,
    updated_at: null
  };

  private sugerencias$ = new BehaviorSubject(this.sugerenciasService.getData());

  dataSource$: Observable<any[]>;
  columns = [
    'ver',
    'categoria_id',
    'created_at',
    'updated_at',
    'opciones'
  ];
  closeResult: string;
  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  pageSize = 10;
  search: string;


  private productos$ = new BehaviorSubject(this.productosService.getData());
  private categorias$ = new BehaviorSubject(this.categoriasService.getData());

  pDataSource$: Observable<any[]>;
  categoriasList: Observable<any[]>;
  pColumns = [
    'id',
    'nombre',
    'categoria_id',
    'precio',
    'disponible',
    'opciones'
  ];
  pDColumns = [
    'id',
    'nombre',
    'categoria_id',
    'precio',
    'disponible'
  ];
  pCurrentPage$ = new BehaviorSubject(1);
  pDataOnPage$: Observable<any[]>;


  constructor(
    private modalService: NgbModal,
    private sugerenciasService: SugerenciasService,
    private productosService: ProductosService,
    private categoriasService: CategoriasService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.dataSource$ = this.sugerencias$.value.pipe(map(v =>  Object.values(v)));
    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.sugerencias$.value),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );

    // productos / categorias

    this.pDataSource$ = this.productos$.value.pipe(map(v =>  Object.values(v)));
    this.pDataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.productos$.value),
      map( v => {
        const idx = (this.pCurrentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
    this.categoriasList = this.categorias$.value.pipe(map(v =>  Object.values(v)));

  }
  open(content) {
    this.resetForm();
    this.loadData();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  editData(content, sugerencia) {
    this.resetForm();
    this.loadData();
    this.sugerencia = sugerencia;
    this.sugerencia.detalle = sugerencia.detalle.map(el => el.producto_id);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }
  deleteData(content, sugerencia) {
    if (confirm('Are you sure you want to delete?')) {
      this.sugerenciasService.deleteData(sugerencia.id).subscribe();
    }
    this.loadData();
    this.modalService.dismissAll();
  }

  onSubmit(sugerenciaForm) {
    if (this.sugerencia.id === null) {
      this.sugerenciasService.insertData(this.sugerencia).subscribe();
    } else {
      this.sugerenciasService.updateData(this.sugerencia).subscribe();
    }
    this.loadData();
    this.resetForm();
    this.modalService.dismissAll();
  }

  resetForm() {
    this.sugerencia = {
      id: null,
      categoria_id: null,
      detalle: [],
      created_at: null,
      updated_at: null
    };
  }

  seleccionar(productoId) {
    const encontrado = this.sugerencia.detalle.find( el => el === productoId);
    if (encontrado == null) {
      this.sugerencia.detalle.push(productoId);
    } else {
      this.sugerencia.detalle = this.sugerencia.detalle.filter( el => el !== productoId);
    }
  }

  isInDetalle(prodId) {
    const encontrado = this.sugerencia.detalle.find( el => el === prodId);
    return encontrado == null ? false : true;
  }

  onKeyPress($event) {
    this.dataSource$ = this.sugerencias$.value.pipe(map(el => Object.values(el).filter(v => {
      return  v.created_at.includes(this.search)
      || v.updated_at.includes(this.search)
    })));

    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.sugerencias$.value.pipe(
        map(el => Object.values(el)
        .filter(v =>  v.created_at.includes(this.search)
        || v.updated_at.includes(this.search)
        )))
      ),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }

  productos(list, row) {
    const detalles = row.detalle.map(el => el.producto_id);
    this.pDataSource$ = this.productos$.value.pipe(map(el => Object.values(el).filter(v => detalles.includes(v.id))));
    this.pDataOnPage$ = this.pCurrentPage$.pipe(
      switchMap( () => this.productos$.value.pipe(
        map(el => Object.values(el).filter(v => detalles.includes(v.id))))),
        map( v => {
          const idx = (this.currentPage$.value - 1) * this.pageSize;
          return Object.values(v).slice(idx, idx + this.pageSize);
        })
      );
    this.modalService.open(list, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

}
