import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CdkTableModule } from '@angular/cdk/table';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from './roles.component';
import { RolesService } from '../../services/roles.service';
// import { UsersService } from '../../services/users.service';
@NgModule({
  imports: [
    CommonModule,
    RolesRoutingModule,
    CdkTableModule,
    NgbModule,
    FormsModule
  ],
  declarations: [
    RolesComponent
   ],
  exports: [],
  providers: [
    RolesService
  ]
})
export class RolesModule { }
