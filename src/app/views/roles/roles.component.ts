import { Component, OnInit } from '@angular/core';
import { RolesService } from '../../services/roles.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { TokenService } from '../../services/token.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  rol = {
    id: null,
    name: null,
    guard_name: null
  };

  headers = new HttpHeaders({
    'Accept': 'application/json;charset=utf-8',
  'Content-Type': 'application/json;charset=utf-8',
    'Authorization': `Bearer ${this.token.get()}`
  });

  private roles$ = new BehaviorSubject(this.rolesService.getData(this.headers));

  dataSource$: Observable<any[]>;
  columns = [
    // 'id',
    'name'
  ];
  closeResult: string;
  currentPage$ = new BehaviorSubject(1);
  dataOnPage$: Observable<any[]>;
  pageSize = 10;

  constructor(
    private modalService: NgbModal,
    private rolesService: RolesService,
    private token: TokenService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.dataSource$ = this.roles$.value.pipe(map(v =>  Object.values(v)));
    this.dataOnPage$ = this.currentPage$.pipe(
      switchMap( () => this.roles$.value),
      map( v => {
        const idx = (this.currentPage$.value - 1) * this.pageSize;
        return Object.values(v).slice(idx, idx + this.pageSize);
      })
    );
  }
  open(content) {
    this.resetForm();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }

  editData(content, rol) {
    this.resetForm();
    this.rol = rol;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'});
  }
  deleteData(content, rol) {
    if (confirm('Are you sure you want to delete?')) {
      this.rolesService.deleteData(rol.id).subscribe();
    }
    this.loadData();
    this.modalService.dismissAll();
  }

  onSubmit(roleForm) {
    if (this.rol.id === null) {
      this.rolesService.insertData(this.rol).subscribe();
    } else {
      this.rolesService.updateData(this.rol).subscribe();
    }
    this.loadData();
    this.resetForm();
    this.modalService.dismissAll();
  }

  resetForm() {
    this.rol = {
      id: null,
      name: null,
      guard_name: null,
    };
  }
}
