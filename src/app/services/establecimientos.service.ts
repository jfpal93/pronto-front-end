import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class EstablecimientosService {
  private headers: HttpHeaders;
  private BASE_URL = environment.BASE_URL;
  constructor(
    private http: HttpClient,
    private token: TokenService
    ) {
    this.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token.get()}`
    });
   }

  handle(data) {
    this.set(data);
  }

  set(data) {
    localStorage.setItem('establecimiento', data);
  }

  getEstablecimiento() {
    return localStorage.getItem('establecimiento');
  }

  remove() {
    localStorage.removeItem('establecimiento');
  }

  insertData(data) {
    return this.http.post(`${this.BASE_URL}/establecimientos`, data, { headers: this.headers });
  }

  updateData(data) {
    return this.http.put(`${this.BASE_URL}/establecimientos/${data.id}`, data, { headers: this.headers });
  }

  getData() {
    return this.http.get(`${this.BASE_URL}/establecimientos`, { headers: this.headers });
  }

  getDataById(id) {
    return this.http.get(`${this.BASE_URL}/establecimientos/${id}`, { headers: this.headers });
  }

  deleteData(id) {
    return this.http.delete(`${this.BASE_URL}/establecimientos/${id}`, { headers: this.headers });
  }

  getPosition(): Promise<any>
  {
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(resp => {
          resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
        },
        err => {
          reject(err);
        });
    });

  }
}
