import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { EstablecimientosService } from './establecimientos.service';
import { TokenService } from './token.service';
import { RolesService } from './roles.service';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private headers: HttpHeaders;
  private BASE_URL = environment.BASE_URL;
  private establecimiento = null;
  constructor(
    private http: HttpClient,
    private est: EstablecimientosService,
    private role: RolesService,
    private token: TokenService
    ) {
      this.establecimiento = est.getEstablecimiento();
      this.headers = new HttpHeaders({
        'Authorization': `Bearer ${this.token.get()}`
      });
  }

  insertData(data) {
    // //console.log(data);
    
    return this.http.post(`${this.BASE_URL}/productos`, data, { headers: this.headers });

  }

  updateData(data) {
    return this.http.put(`${this.BASE_URL}/productos/${data.id}`, data, { headers: this.headers });
  }

  getData() {
    if (this.role.getRole() !== 'admin') {
      return this.http.get(`${this.BASE_URL}/establecimiento/${this.establecimiento}/productos`, { headers: this.headers });
    } else {
      return this.http.get(`${this.BASE_URL}/productos`, { headers: this.headers });
    }
  }

  getDataById(id) {
    return this.http.get(`${this.BASE_URL}/productos/${id}`, { headers: this.headers });
  }

  deleteData(id) {
    return this.http.delete(`${this.BASE_URL}/productos/${id}`, { headers: this.headers });
  }
}
