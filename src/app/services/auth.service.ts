import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { TokenService } from './token.service';
import { RolesService } from './roles.service';
import { EstablecimientosService } from './establecimientos.service';
import { environment } from '../../environments/environment';
import { MessagingService } from '../shared/messaging.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private BASE_URL = environment.BASE_URL;
  private loggedIn = new BehaviorSubject<boolean>(this.token.loggedIn());
  authStatus = this.loggedIn.asObservable();
  private headers: HttpHeaders;
  constructor(
    private http: HttpClient,
    private token: TokenService,
    private role: RolesService,
    private est: EstablecimientosService,
    private messagingService: MessagingService,
    ) {
      this.headers = new HttpHeaders({
      //   'Accept': 'application/json;charset=utf-8',
      // 'Content-Type': 'application/json;charset=utf-8',
        'Authorization': `Bearer ${this.token.get()}`
      });
    }

  login(data) {
    let head:HttpHeaders= new HttpHeaders({
      'Accept': 'application/json;charset=utf-8',
      'Content-Type': 'application/json;charset=utf-8'
    });
    return this.http.post(`${this.BASE_URL}/login`, data, { headers: head });
  }

  logout(headers:HttpHeaders) {
    let token=this.retrieveFireToken();
    
    return this.http.post(`${this.BASE_URL}/logout`,{firebase_token:token.firebase_token}, { headers: headers }).subscribe((data)=>{
      localStorage.clear();
    });
  }

  register(data) {
    return this.http.post(`${this.BASE_URL}/register`, data);
  }

  handleUser(data) {
    this.setUser(data);
  }

  setUser(data) {
    localStorage.setItem('user', JSON.stringify(data) );
  }

  removeUser() {
    localStorage.removeItem('user');
  }
  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  changeAuthStatus(value: boolean) {
    this.removeUser();
    this.role.remove();
    this.token.remove();
    this.est.remove();
    this.loggedIn.next(value);
  }

  registerFirebaseToken(data,token, firetoken){
    let head= new HttpHeaders({
      'Accept': 'application/json;charset=utf-8',
      'Content-Type': 'application/json;charset=utf-8',
      'Authorization': `Bearer ${token}`
    });
    
    let fire={
      firebase_token:null
    }
    // this.messagingService.requestPermission().subscribe(
    // (token) => {
      fire.firebase_token=firetoken;
      this.http.post(`${this.BASE_URL}/user/${data.id}/token`, fire, { headers: head }).subscribe(
      (data)=>{
        this.setFireToken();
        this.saveFireToken(firetoken);
      });
    // },
    // (err) => {
    // })
  }
  
  setFireToken() {
    localStorage.setItem('firetoken', JSON.stringify({"firetoken":true}) );
  }

  saveFireToken(token:any) {
    localStorage.setItem('firebase_token', JSON.stringify({"firebase_token":token}) );
  }

  deleteFireToken(token:any) {
    localStorage.removeItem('firebase_token');
  }

  removeFireToken() {
    localStorage.removeItem('firetoken');
  }
  getFireToken() {
    return JSON.parse(localStorage.getItem('firetoken'));
  }

  retrieveFireToken() {
    return JSON.parse(localStorage.getItem('firebase_token'));
  }


  
}
