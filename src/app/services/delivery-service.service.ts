import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TokenService } from './token.service';
import { environment } from '../../environments/environment';
import { RolesService } from './roles.service';

@Injectable({
  providedIn: 'root'
})
export class DeliveryServiceService {

  private BASE_URL = environment.BASE_URL;
  private headers: HttpHeaders;

  private estabId;

  constructor(
    private http: HttpClient,
    private token: TokenService,
    private role: RolesService
  ) {
    this.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token.get()}`
    });

    this.estabId=JSON.parse(localStorage.getItem('establecimiento'));

   }

  getData() {
    
    if (this.role.getRole() !== 'admin' && this.role.getRole() !== 'secretarioGlobal') {
      return this.http.get(`${this.BASE_URL}/getPedidosEstab/${this.estabId}`, { headers: this.headers });
    }
    else{
      return this.http.get(`${this.BASE_URL}/getOrdenesTS`, { headers: this.headers });
    }
  }

  getRepartidores(){
    if (this.role.getRole() == 'admin' || this.role.getRole() == 'secretarioGlobal') {          
      return this.http.get(`${this.BASE_URL}/repartidores`, { headers: this.headers });
    }
    
  }
  

  selectTime(data) {
    return this.http.post(`${this.BASE_URL}/selectCookTime/${this.estabId}`,data, { headers: this.headers });
  }

  setTimePriceDelOrder(data) {
    if (this.role.getRole() == 'admin' || this.role.getRole() == 'secretarioGlobal') {          

      return this.http.post(`${this.BASE_URL}/setTimePriceDelOrder`,data, { headers: this.headers });
    }
  }

  setRepOnly(data){
    if (this.role.getRole() == 'admin' || this.role.getRole() == 'secretarioGlobal') {          

      return this.http.post(`${this.BASE_URL}/setRepOnly`,data, { headers: this.headers });
    }
  }
}
