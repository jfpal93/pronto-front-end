import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class PublicidadesService {

  private headers: HttpHeaders;
  private BASE_URL = environment.BASE_URL;
  constructor(
    private http: HttpClient,
    private token: TokenService
    ) {
    this.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token.get()}`
    });
  }

  insertData(data) {
    return this.http.post(`${this.BASE_URL}/publicidads`, data, { headers: this.headers });
  }

  updateData(data) {
    return this.http.put(`${this.BASE_URL}/publicidads/${data.id}`, data, { headers: this.headers });
  }

  getData() {
    return this.http.get(`${this.BASE_URL}/publicidads`, { headers: this.headers });
  }

  getDataById(id) {
    return this.http.get(`${this.BASE_URL}/publicidads/${id}`, { headers: this.headers });
  }

  deleteData(id) {
    return this.http.delete(`${this.BASE_URL}/publicidads/${id}`, { headers: this.headers });
  }
}
