import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  private headers: HttpHeaders;
  private BASE_URL = environment.BASE_URL;
  constructor(
    private http: HttpClient, 
    private token: TokenService
    ) {
    this.headers = new HttpHeaders({
      'Accept': 'application/json;charset=utf-8',
      'Content-Type': 'application/json;charset=utf-8',
      'Authorization': `Bearer ${this.token.get()}`
    });
   }

  handle(data) {
    this.set(data);
  }

  set(data) {
    localStorage.setItem('role', data);
  }

  getRole() {
    return localStorage.getItem('role');
  }

  remove() {
    localStorage.removeItem('role');
  }

  isRole(role) {
    if(this.getRole() === role) {
      return true;
    } else {
      return false;
    }
  }


  insertData(data) {
    return this.http.post(`${this.BASE_URL}/roles`, data, { headers: this.headers });
  }

  updateData(data) {
    return this.http.put(`${this.BASE_URL}/roles/${data.id}`, data, { headers: this.headers });
  }

  getData(headers:HttpHeaders) {
    return this.http.get(`${this.BASE_URL}/roles`, { headers: headers });
  }

  getDataById(id) {
    return this.http.get(`${this.BASE_URL}/roles/${id}`, { headers: this.headers });
  }

  deleteData(id) {
    return this.http.delete(`${this.BASE_URL}/roles/${id}`, { headers: this.headers });
  }
}
