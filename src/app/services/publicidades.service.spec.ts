import { TestBed } from '@angular/core/testing';

import { PublicidadesService } from './publicidades.service';

describe('PublicidadesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublicidadesService = TestBed.get(PublicidadesService);
    expect(service).toBeTruthy();
  });
});
