import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private headers: HttpHeaders;
  private BASE_URL = environment.BASE_URL;
  constructor(
    private http: HttpClient,
    private token: TokenService
    ) {
    this.headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token.get()}`
    });
   }


  getData() {
    return this.http.get(`${this.BASE_URL}/users`, { headers: this.headers });
  }

  getDataById(id) {
    return this.http.get(`${this.BASE_URL}/users/${id}`, { headers: this.headers });
  }

  updateData(data) {
    return this.http.put(`${this.BASE_URL}/users/${data.id}`, data, { headers: this.headers });
  }

  deleteData(id) {
    return this.http.delete(`${this.BASE_URL}/users/${id}`, { headers: this.headers });
  }
}
