import { Pipe, PipeTransform } from '@angular/core';
import { RolesService } from '../services/roles.service';

@Pipe({
  name: 'navItems'
})
export class NavItemsPipe implements PipeTransform {

  roles = [
    {
      name: 'admin',
      items: [
        'Dashboard',
        'Limite',
        'Usuarios',
        'Roles',
        'Categorias',
        'Establecimientos',
        'Favoritos',
        'Publicidades',
        'Sugerencias',
        'Delivery',
      ]
    },
    {
      name: 'adminEstablecimiento',
      items: [
        'Dashboard',
        'Productos',
        'Delivery',
      ]
    },
    {
      name: 'secretarioEstablecimiento',
      items: [
        'Dashboard',
        'Delivery',
      ]
    },
    {
      name: 'secretarioGlobal',
      items: [
        'Dashboard',
        'Delivery'
      ]
    },
  ];

  constructor(private roleService: RolesService) {

  }

  transform(value: any, ...args: any[]): any {
    const role = this.roleService.getRole();
    const permissions = this.roles.filter( el => el.name === role).map(el => el.items)[0];
    const arr = value.filter(el => permissions.includes(el.name));
    return  arr;
  }

}
