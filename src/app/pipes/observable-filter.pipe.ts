import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'observableFilter'
})
export class ObservableFilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    return value.find(el => el.id === arg);
  }

}
