import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { AuthGuard } from './guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'users',
        loadChildren: () => import('./views/users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'roles',
        loadChildren: () => import('./views/roles/roles.module').then(m => m.RolesModule)
      },
      {
        path: 'categorias',
        loadChildren: () => import('./views/categorias/categorias.module').then(m => m.CategoriasModule)
      },
      {
        path: 'establecimientos',
        loadChildren: () => import('./views/establecimientos/establecimientos.module').then(m => m.EstablecimientosModule)
      },
      {
        path: 'favoritos',
        loadChildren: () => import('./views/favoritos/favoritos.module').then(m => m.FavoritosModule)
      },
      {
        path: 'productos',
        loadChildren: () => import('./views/productos/productos.module').then(m => m.ProductosModule)
      },
      {
        path: 'publicidades',
        loadChildren: () => import('./views/publicidades/publicidades.module').then(m => m.PublicidadesModule)
      },
      {
        path: 'sugerencias',
        loadChildren: () => import('./views/sugerencias/sugerencias.module').then(m => m.SugerenciasModule)
      },
      {
        path: 'delivery',
        loadChildren: () => import('./views/delivery/delivery.module').then(m => m.DeliveryModule)
      },
      {
        path: 'limite',
        loadChildren: () => import('./views/limite/limite.module').then(m => m.LimiteModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
