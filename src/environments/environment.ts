// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyD2opFRXJvehdjDso8qwITaSlWUrp17Sjo",
    authDomain: "prontoapp-5cec2.firebaseapp.com",
    databaseURL: "https://prontoapp-5cec2.firebaseio.com",
    projectId: "prontoapp-5cec2",
    storageBucket: "prontoapp-5cec2.appspot.com",
    messagingSenderId: "927231912697",
    appId: "1:927231912697:web:0f6c2a2899dd2d28"
  },
  BASE_URL:  'https://prontoapi.vitiks.com/api/api'
  // BASE_URL:  'http://localhost/pronto-back-end/public/api'
};
